#pragma once
namespace WorkingWithArrays
{
	using key = int (*)(int);
	using predicate = bool (*)(int);

	int* allocateMemory(int);
	void defaultValues(int*, int);
	void setValues(int*, int, int);
	void randomValues(int*, int);

	int* resizeArray(int*, int, int);

	void displayArray(int*, int);

	int* addToEnd(int*, int, int);
	int* removeFromEnd(int*, int);
	int* addToStart(int*, int, int);
	int* removeFromStart(int*, int);
	int* addToPosition(int*, int, int, int);
	int* removeFromPosition(int*, int, int);

	double averageValue(double*, int);
	double sumValues(double*, int);
	int maxValue(int*, int);
	int minValue(int*, int);
	int* transformArray(int*, int, key);
	int key1(int);

	int* filterByPredicate(int*, int, int&, predicate);
	bool predicate1(int);
}