﻿#include "homework.h"
#include "iostream"

int* WorkingWithArrays::allocateMemory(int n)
{
    if (n <= 0)
    {
        return nullptr;
    }

    int* p = new int[n];
    return p;
}


void WorkingWithArrays::defaultValues(int* p, int n)
{
    if (n <= 0)
    {
        return;
    }
    for (int* q = p; q < p + n; q++)
    {
        *q = 1;
    }
}

void WorkingWithArrays::setValues(int* p, int n, int defaultValue)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    for (int* q = p; q < p + n; q++)
    {
        *q = defaultValue;
    }
}

int* WorkingWithArrays::addToEnd(int* p, int value, int length)
{
    int* q = allocateMemory(length + 1);

    for (int* r = p; r < p + length; r++, q++)
    {
        *q = *r;
    }

    *++q = value;

    delete[]p;
    return q;
}

int* WorkingWithArrays::removeFromEnd(int* p, int length)
{
    int* q = allocateMemory(length - 1);

    for (int* r = p; r < p + length - 1; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::addToStart(int* p, int value, int length)
{
    int* q = allocateMemory(length + 1);

    *q++ = value;
    p++;

    for (int* r = p; r < p + length; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::removeFromStart(int* p, int length)
{
    int* q = allocateMemory(length - 1);

    for (int* r = ++p; r < p + length; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::addToPosition(int* p, int value, int position, int length)
{
    int* q = allocateMemory(length + 1);

    for (int* r = p; r < p + position; r++, q++)
    {
        *q = *r;
    }

    *++q = value;

    for (int* r = ++p; r < p + length + 1; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::removeFromPosition(int* p, int position, int length)
{
    int* q = allocateMemory(length);

    for (int* r = p; r < p + position; r++, q++)
    {
        *q = *r;
    }

    q++;

    for (int* r = ++p; r < p + length - 1; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

double WorkingWithArrays::averageValue(double* p, int length)
{
    double sum = 0;

    for (double* r = p; r < p + length; r++)
    {
        sum += *r;
    }

    return sum / length;
}

double WorkingWithArrays::sumValues(double* p, int length)
{
    double sum = 0;

    for (double* r = p; r < p + length; r++)
    {
        sum += *r;
    }

    return sum;
}

int WorkingWithArrays::maxValue(int* p, int length)
{
    int currentMax = *p;

    for (int* r = ++p; r < p + length; r++)
    {
        if (*r > currentMax)
        {
            currentMax = *r;
        }
    }

    return currentMax;
}

int WorkingWithArrays::minValue(int* p, int length)
{
    int currentMin = *p;

    for (int* r = ++p; r < p + length; r++)
    {
        if (*r < currentMin)
        {
            currentMin = *r;
        }
    }

    return currentMin;
}

int* WorkingWithArrays::transformArray(int* p, int length, key keyfn)
{
    int* q = allocateMemory(length);

    for (int* r = p; r < p + length; r++, q++)
    {
        *q = keyfn(*r);
    }
}

int WorkingWithArrays::key1(int number)
{
    return (abs(number) - number * 2) + 100;
}

void WorkingWithArrays::randomValues(int* p, int n)
{
    if (n <= 0)
    {
        return;
    }

    srand(time(nullptr));

    for (int* q = p; q < p + n; q++)
    {
        *q = rand() % 100;
    }
}


void WorkingWithArrays::displayArray(int* p, int n)
{
    if (n <= 0)
    {
        return;
    }

    for (int* q = p; q < p + n; q++)
    {
        std::cout << *q << " ";
    }
}

int* WorkingWithArrays::resizeArray(int* p, int oldlength, int newlength)
{
    int* q = allocateMemory(newlength);

    for (int* r = p; r < p + oldlength; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::filterByPredicate(int* p, int length, int& newLength, predicate func)
{
    newLength = 0;

    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* result = allocateMemory(length);

    for (int* r = p, *q = result; r < p + length; r++)
    {
        if (func(*r))
        {
            *q = *r;
            q++;
            newLength++;
        }
    }

    delete[] p;
    return newLength == length ? result : resizeArray(result, length, newLength);
}

bool WorkingWithArrays::predicate1(int number)
{
    return number > 0;
}
