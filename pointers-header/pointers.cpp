#include <iostream>
#include "arrays.h"

using namespace std;
int main()
{
    int n;
    cin >> n;

    int* array = WorkingWithArrays::allocateMemoryInt(n);
    WorkingWithArrays::randomValues(array, n);
    WorkingWithArrays::displayArrayInt(array, n);
    // тестирование resize
    /*cout << endl;*/
 
    //try
    //{
    //    WorkingWithArrays::allocateMemory(9223372036854775807);

    //}
    //catch (const std::exception&)
    //{
    //    cout << "Problems with memory allocation";
    //}    
    //cout << endl;
    //int k = 0;
    //int* a = WorkingWithArrays::filterByPredicate(array, n, k, WorkingWithArrays:: isOdd);
    //cout << endl;
    //WorkingWithArrays::displayArrayInt(a, k);
    //cout << endl;

    //a = WorkingWithArrays::filterByPredicate(a, k, k, WorkingWithArrays::isEven);
    //cout << endl;
    //WorkingWithArrays::displayArray(a, k);
    //cout << endl;    

    //a = WorkingWithArrays::filterByPredicate(a, k, k, WorkingWithArrays::isPositive);
    //WorkingWithArrays::displayArrayInt(a, k);
    cout << endl;
    cout << endl;
    WorkingWithArrays::bubbleSort(array, n, WorkingWithArrays::key1);
    WorkingWithArrays::displayArrayInt(array, n);
}
