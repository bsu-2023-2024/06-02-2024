#include "arrays.h"
#include "iostream"

int* WorkingWithArrays::allocateMemoryInt(int n)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    return new int[n];
}

double* WorkingWithArrays::allocateMemoryDouble(int n)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    return new double[n];
}

void WorkingWithArrays::defaultValues(int* p, int n)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    for (int* q = p; q < p + n; q++)
    {
        *q = 1;
    }
}

int* WorkingWithArrays::addToEnd(int* p, int value, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* q = allocateMemoryInt(length + 1);

    for (int* r = p; r < p + length; r++, q++)
    {
        *q = *r;
    }

    *++q = value;

    delete[]p;
    return q;
}

int* WorkingWithArrays::removeFromEnd(int* p, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* q = allocateMemoryInt(length - 1);

    for (int* r = p; r < p + length - 1; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::addToStart(int* p, int value, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* q = allocateMemoryInt(length + 1);

    *q++ = value;
    p++;

    for (int* r = p; r < p + length; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::removeFromStart(int* p, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* q = allocateMemoryInt(length - 1);

    for (int* r = ++p; r < p + length; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::addToPosition(int* p, int value, int position, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* q = allocateMemoryInt(length + 1);

    for (int* r = p; r < p + position; r++, q++)
    {
        *q = *r;
    }

    *++q = value;

    for (int* r = ++p; r < p + length + 1; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

int* WorkingWithArrays::removeFromPosition(int* p, int position, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* q = allocateMemoryInt(length);

    for (int* r = p; r < p + position; r++, q++)
    {
        *q = *r;
    }

    q++;
    
    for (int* r = ++p; r < p + length - 1; r++, q++)
    {
        *q = *r;
    }

    delete[]p;
    return q;
}

double WorkingWithArrays::averageValue(double* p , int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    double sum = 0;

    for (double* r = p; r < p + length; r++)
    {
        sum += *r;
    }

    return sum / length;
}

double WorkingWithArrays::sumValues(double* p, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    double sum = 0;

    for (double* r = p; r < p + length; r++)
    {
        sum += *r;
    }

    return sum;
}

int WorkingWithArrays::maxValue(int* p, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int currentMax = *p;

    for (int* r = ++p; r < p + length; r++)
    {
        if (*r > currentMax)
        {
            currentMax = *r;
        }
    }

    return currentMax;
}

int WorkingWithArrays::minValue(int* p, int length)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int currentMin = *p;

    for (int* r = ++p; r < p + length; r++)
    {
        if (*r < currentMin)
        {
            currentMin = *r;
        }
    }

    return currentMin;
}

int* WorkingWithArrays::filterByPredicate(int* p, int length, int& newLength, predicate func)
{
    newLength = 0;

    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    int* result = allocateMemoryInt(length);

    // �������� - ��������� ����� ��� ������ ���������, �������������� ��������
    for (int* r = p, *q = result; r < p + length; r++)
    {
        if (func(*r))
        {
            *q = *r;
            q++;
            newLength++;
        }
    }

    //if (newLength != length)
    //{
    //    extendArray(result, length, newLength);
    //}

    return newLength == length ? result : resizeArray(result, length, newLength);
}

bool WorkingWithArrays::isEven(int number)
{
    return !(number % 2);
}

bool WorkingWithArrays::isOdd(int number)
{
    return number % 2;
}

bool WorkingWithArrays::isPositive(int number)
{
    return number > 0;
}

double* WorkingWithArrays::transformArray(double* p, int length, operation func)
{
    if (length <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    double* q = new double[length];

    double* s = q;
    for (double* r = p ; r < p + length; r++, s++)
    {
        *s = func(*r);
    }

    return q;
}

double WorkingWithArrays::operation1(double num)
{
    return sqrt(sin(num) - cos(num));
}

void WorkingWithArrays::bubbleSort(int* array, int length, key key)
{
    int* keys = new int[length];

    for (int i = 0; i < length; i++)
    {
        keys[i] = key(array[i]);
    }

    for (int i = 0; i < length; i++)
    {
        for (int j = length - 1; j > i; j--)
        {
            if (keys[j] < keys[j - 1])
            {
                std::cout << "asd " << array[j] << " " << keys[j] << std::endl;
                int tmp = array[j];
                array[j] = array[j - 1];
                array[j - 1] = tmp;

                tmp = keys[j];
                keys[j] = keys[j - 1];
                keys[j - 1] = tmp;
            }
        }
    }
}

void WorkingWithArrays::bubbleSort(int* arr, int length, comparer comparer)
{
    for (size_t i = 0; i < length; i++)
    {
        for (size_t j = length; j > i; j--)
        {
            if (comparer(arr[j], arr[j - 1]) < 0)
            {
                int tmp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = tmp;
            }
        }
    }
}

int WorkingWithArrays::key1(int number)
{
    return number;
}

int WorkingWithArrays::key2(int number)
{
    return abs(number);
}

int WorkingWithArrays::getLength(int n)
{
    int i = 0;
    while (n)
    {
        n /= 10;
        i++;
    }
    return i;
}

int WorkingWithArrays::compareByAbsValue(int n1, int n2)
{
    return abs(n1) - abs(n2);
}

void WorkingWithArrays::randomValues(int* p, int n)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    srand(time(nullptr));

    for (int* q = p; q < p + n; q++)
    {
        *q = rand() % 100 - 50;
    }
}


void WorkingWithArrays::displayArrayInt(int* p, int n)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    for (int* q = p; q < p + n; q++)
    {
        std::cout << *q << " ";
    }
}


void WorkingWithArrays::displayArrayDouble(double* p, int n)
{
    if (n <= 0)
    {
        throw std::out_of_range("Array length must be more than 0");
    }

    for (double* q = p; q < p + n; q++)
    {
        std::cout << *q << " ";
    }
}

int* WorkingWithArrays::resizeArray(int* p, int oldLength, int newLength)
{
    if (newLength == oldLength)
    {
        return p;
    }

    int* q = allocateMemoryInt(newLength);

    for (int* r = p, *s = q; r < p + oldLength; r++, s++)
    {
        *s = *r;
    }

    delete[]p;
    return q;
}


void WorkingWithArrays::extendArray(int*& p, int oldLength, int newLength)
{
    if (newLength < oldLength)
    {
        throw std::invalid_argument("Length of new array can't be less than old");
    }
    if (newLength == oldLength)
    {
        return;
    }
    int* q = allocateMemoryInt(newLength);

    for (int* r = p, *s = q; r < p + oldLength; r++, s++)
    {
        *s = *r;
    }
 
    delete[]p;
    p = q;
}
