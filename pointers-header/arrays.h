#pragma once
namespace WorkingWithArrays
{
	using predicate = bool (*)(int);
	using operation = double (*)(double);
	using key = int (*)(int);
	using comparer = int (*)(int, int);

	int* allocateMemoryInt(int);
	double* allocateMemoryDouble(int);
	void randomValues(int*, int);
	void displayArrayInt(int*, int);
	void displayArrayDouble(double*, int);
	int* resizeArray(int*, int, int);
	void extendArray(int*&, int, int);
	void defaultValues(int*, int);
	int* addToEnd(int*, int, int);
	int* removeFromEnd(int*, int);
	int* addToStart(int*, int, int);
	int* removeFromStart(int*, int);
	int* addToPosition(int*, int, int, int);
	int* removeFromPosition(int*, int, int);
	double averageValue(double*, int);
	double sumValues(double*, int);
	int maxValue(int*, int);
	int minValue(int*, int);
	int* filterByPredicate(int*, int, int&, predicate);
	bool isEven(int);
	bool isOdd(int);
	bool isPositive(int);
	double* transformArray(double*, int, operation);
	double operation1(double);
	void bubbleSort(int*, int, key);
	void bubbleSort(int*, int, comparer);
	int key1(int);
	int key2(int);
	int getLength(int);
	int compareByAbsValue(int, int);
}
